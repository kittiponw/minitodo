from django.http import Http404

from api.models import TODOTask
from api.serializers import TODOTaskSerializer

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status

class TaskList ( APIView ):
    """
    List all todo tasks, or create a new todo task.
    """
    def get ( self, request, format=None ):
        tasks = TODOTask.objects.all()
        serializer = TODOTaskSerializer( tasks, many=True )
        return Response( serializer.data )
    
    def post ( self, request, format=None ):
        serializer = TODOTaskSerializer( data=request.data )
        if serializer.is_valid():
            serializer.save()
            return Response( serializer.data, status=status.HTTP_201_CREATED )
        else:
            return Response( serializer.errors, status=status.HTTP_400_BAD_REQUEST )
        
class TaskDetail ( APIView ):
    """
    Retrieve, update or delete a todo task.
    """
    def get_object ( self, task_id ):
        try:
            return TODOTask.objects.get( id=task_id )
        except TODOTask.DoesNotExist:
            raise Http404
        
    def get ( self, request, task_id, format=None ):
        task = self.get_object( task_id )
        serializer = TODOTaskSerializer( task )
        return Response( serializer.data )
    
    def put ( self, request, task_id, format=None ):
        task = self.get_object( task_id )
        serializer = TODOTaskSerializer( task, data=request.data )
        if serializer.is_valid():
            serializer.save()
            return Response( serializer.data )
        else:
            return Response( serializer.errors, status=status.HTTP_400_BAD_REQUEST )
        
    def delete ( self, request, task_id, format=None ):
        task = self.get_object( task_id )
        task.delete()
        return Response( status=status.HTTP_204_NO_CONTENT )
    