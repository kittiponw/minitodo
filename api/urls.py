from django.conf.urls import url
from django.views.generic import RedirectView

from api import views

from rest_framework.urlpatterns import format_suffix_patterns

urlpatterns = [
    url( r'^$', RedirectView.as_view( url='/api/tasks/') ),
    url( r'^tasks/$', views.TaskList.as_view() ),
    url( r'^tasks/(?P<task_id>[0-9]+)/$', views.TaskDetail.as_view() ),
]

urlpatterns = format_suffix_patterns( urlpatterns )

