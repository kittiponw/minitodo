from rest_framework import serializers
from api.models import TODOTask

class TODOTaskSerializer ( serializers.ModelSerializer ):
    class Meta:
        model = TODOTask
        fields = ( 'id', 'subject', 'description', 'completed' )