
from __future__ import unicode_literals

from django.db import models

class TODOTask ( models.Model ):
    subject = models.CharField( max_length=100, blank=True, default='' )
    description = models.TextField( blank=True, default='' )
    completed = models.BooleanField( default=False )