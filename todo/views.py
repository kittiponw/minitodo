from django.shortcuts import render

from django.http import HttpResponse

import requests
import json

def index ( request ):
    req = requests.get( 'http://localhost:8080/api/tasks/' )
    content = json.loads( req.content )
    return render( request, 'todo/index.html', { 'tasks': content } )
